'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}



function main() {
    const N = parseInt(readLine(), 10);
    const W = "Weird";
    const NW = "Not Weird";

    if (N % 2 !== 0) { //If  is odd, print Weird
        console.log(W);
    } else if (N % 2 === 0) {
        if (Array(4).fill(2).map((x, y) => x + y).includes(N)) {//If  is even and in the inclusive range of 2 to 5, print Not Weird
            console.log(NW);
        }
        if (Array(15).fill(6).map((x, y) => x + y).includes(N)) { //If  is even and in the inclusive range of 6 to 20, print Weird
            console.log(W);
        }

        if (N > 20) { // If  is even and greater than 20, print Not Weird
            console.log(NW);
        }

    }

}