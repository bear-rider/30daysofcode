function processData(input) {
    //Enter your code here
    let arr = input.split('\n');
    arr.reverse().pop();
    arr.reverse();
    arr.forEach(function(word) {
        const odds = [];
        const evens = [];
        const chars = [...word];
        for (let i = 0; i < chars.length; i++) {
            if (i%2 === 0){
                evens.push(chars[i]);
            } else {
                odds.push(chars[i]);
            }
        }
        console.log(evens.join('')+" "+odds.join(''));
    });

} 

process.stdin.resume();
process.stdin.setEncoding("ascii");
_input = "";
process.stdin.on("data", function (input) {
    _input += input;
});

process.stdin.on("end", function () {
   processData(_input);
});